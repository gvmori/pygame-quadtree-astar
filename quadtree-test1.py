## test for generating quadtrees from pygame surfaces
## to be used with A* pathfinding
## to generate smooth, pixel-perfect pathfinding on non-tile-based images

## this won't work at all.

import sys
import math

import time

import pygame
from pygame.locals import *

# import cProfile


screen = pygame.display.get_surface()

WALKABLE_COLOR = (255,0,255)
WALKABLE_COLOR_INT = None

QUAD_SIZE_CUTOFF = 3

QUAD_DEPTH_MIN = 6
QUAD_DEPTH_CUTOFF = 9

QUAD_QUEUE = []

def main():
    global WALKABLE_COLOR_INT
    pygame.init()
    bkg_img = pygame.image.load("./test_bg_3.png")
    bkg_img_walk_mask = pygame.image.load("./test_bg_3_mask.bmp")

    bkg_walk_array = pygame.surfarray.pixels2d(bkg_img_walk_mask)

    WALKABLE_COLOR_INT = bkg_img_walk_mask.map_rgb(WALKABLE_COLOR)

    WINDOW = pygame.display.set_mode((1280, 800))

    # WINDOW.blit(bkg_img, (0,0))

    game_window = GameWindow(WINDOW, pygame.Surface((1280,800)), bkg_img)

    t = time.clock()
    # quadtree = QuadTree(bkg_walk_array, bkg_img_walk_mask.get_rect().right, bkg_img_walk_mask.get_rect().bottom, surf=WINDOW)
    # quadtree = QuadTree(bkg_walk_array, 1280, 800, surf=WINDOW)
    quadtree = QuadTree(bkg_walk_array, 1280, 800, game_window)
    print time.clock() - t
    # sys.exit() ## DEBUG

    pygame.display.update()

    while True:
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            elif event.type == MOUSEBUTTONUP:
                quadtree.handle_click(event)
        # global QUAD_QUEUE
        # if QUAD_QUEUE:
        #     next_rect = QUAD_QUEUE.pop(0)
        #     pygame.draw.rect(*next_rect)
        game_window.update()
        # pygame.display.update()


class GameWindow(object):
    def __init__(self, game_window, draw_surf, bkg_img):
        self.game_window = game_window
        self.draw_surf = draw_surf
        self.bkg_img = bkg_img
        self.need_update = False
        self.blit(self.bkg_img, (0,0))

    def blit(self, surf, dest):
        self.draw_surf.blit(surf, dest)
        # self.draw_surf.fill((0,255,0))
        self.need_update = True

    def draw_rect(self, color, rect, width=0):
        pygame.draw.rect(self.draw_surf, color, rect, width)
        self.need_update = True

    # def clear_rect(self, rect):
    #     print rect
    #     self.draw_surf.set_clip(rect)
    #     self.draw_surf.blit(self.bkg_img, rect)
    #     self.draw_surf.set_clip(None)
    #     self.need_update = True

    def clear(self):
        self.draw_surf.blit(self.bkg_img, (0,0))

    def update(self):
        if self.need_update:
            self.game_window.blit(self.draw_surf, (0,0))
            self.need_update = False
        pygame.display.update()






class QuadTree(object):
    __slots__ = ["orig_array", "max_x", "max_y", "game_window", "root", "selected_quad"]
    def __init__(self, orig_array, max_x, max_y, game_window):
        self.max_x = max_x
        self.max_y = max_y
        self.orig_array = orig_array
        self.root = Quad(self, None, 0, 0, max_x, max_y)
        self.game_window = game_window
        self.root.subdivide(1)
        self.selected_quad = None

    def handle_click(self, event):
        pos = event.pos
        target_quad = self.root.get_target_quad(pos)
        if target_quad and target_quad.get_walkable():
            if self.selected_quad:
                # self.game_window.clear_rect(pygame.Rect((target_quad.left, target_quad.top, target_quad.right-target_quad.left, target_quad.bottom-target_quad.top)))
                self.game_window.clear()
                self.selected_quad.selected = False
                self.selected_quad = None
            #     self.game_window.blit()
            target_quad.selected = True
            # pygame.draw.rect(self.game_window, (0,255,0), pygame.Rect((target_quad.left, target_quad.top, target_quad.right-target_quad.left, target_quad.bottom-target_quad.top)))
            self.game_window.draw_rect((0,255,0), pygame.Rect((target_quad.left, target_quad.top, target_quad.right-target_quad.left, target_quad.bottom-target_quad.top)))
            self.selected_quad = target_quad


class Quad(object):
    __slots__ = ["tree", "parent", "top", "bottom", "left", "right", "_walkable", "children", "selected"]
    def __init__(self, tree, parent, left, top, right, bottom, walkable=None):
        assert top <= bottom
        assert left <= right
        self.tree = tree
        self.parent = parent
        self.top = top
        self.bottom = bottom
        self.left = left
        self.right = right
        # self.rect = pygame.rect.Rect((top, left), (bottom, right))
        self._walkable = walkable
        self.children = []
        self.selected = False

    ## our quads are guaranteed to be aligned with the axes, so we can use simple logic

    ## track the level of depth before calling determine_walkable
    ##      --it's by far the most expensive function in this process, and
    ##        it's much cheaper to call it with smaller array slices when
    ##        possible. diminishing returns seems to set in at level 6/7.
    ##      --additionally, take an early out when the level gets too small
    ##        to matter for gameplay purposes, or extra quads will be generated.
    ##        cutoff will probably depend on final character/level pixel size.
    def subdivide(self, level):
        level += 1
        if level > QUAD_DEPTH_CUTOFF:
            self.set_walkable(False)
            return
        else:
            mid_h_point = ((self.left + self.right) / 2)
            mid_v_point = ((self.top + self.bottom) / 2)
            # if ((mid_h_point - self.left) <= QUAD_SIZE_CUTOFF) or ((mid_v_point - self.top) <= QUAD_SIZE_CUTOFF):
            #     self.set_walkable(False)
            #     return
            children_list = [
                (self.left, self.top, mid_h_point, mid_v_point),
                (mid_h_point, self.top, self.right, mid_v_point),
                (self.left, mid_v_point, mid_h_point, self.bottom),
                (mid_h_point, mid_v_point, self.right, self.bottom),
            ]
            # print children_list
            self.children = [Quad(self.tree, self, *child) for child in children_list]
            for child in self.children:
                if level < QUAD_DEPTH_MIN:
                    child.subdivide(level)
                elif not child.determine_walkable():
                    child.subdivide(level)


    def set_walkable(self, walkable):
        global QUAD_QUEUE
        # if walkable:
        #     # pygame.draw.rect(self.tree.surf, (0,0,255), pygame.Rect((self.left, self.top), (self.right, self.bottom)), 1)
        #     QUAD_QUEUE.append((self.tree.surf, (0,0,255), pygame.Rect((self.left, self.top), (self.right-self.left, self.bottom-self.top)), 1))
        # else:
        #     # pygame.draw.rect(self.tree.surf, (255,0,0), pygame.Rect((self.left, self.top), (self.right, self.bottom)), 1)
        #     QUAD_QUEUE.append((self.tree.surf, (255,0,0), pygame.Rect((self.left, self.top), (self.right-self.left, self.bottom-self.top)), 1))
        self._walkable = walkable

    def get_walkable(self):
        return self._walkable

    def determine_walkable(self):
        subarray = self.tree.orig_array[self.left:self.right, self.top:self.bottom].flatten()
        any_part_walkable = WALKABLE_COLOR_INT in subarray
        ## no parts walkable
        if not any_part_walkable: 
            self.set_walkable(False)
            return True
        ## every part is walkable
        elif len(set(subarray)) == 1:
            self.set_walkable(True)
            return True
        ## some of each
        return False


    def collides(self, x, y):
        return ((self.left <= x <= self.right) and (self.top <= y <= self.bottom))

    def get_target_quad(self, pos):
        if not self.collides(*pos):
            return None
        if not self.children:
            return self
        for child in self.children:
            if child.collides(*pos):
                return child.get_target_quad(pos)

    def get_adjacent(self):
        pass


# b = Quad(None,1,2,3,4)
# b.subdivide()
# print b.children
# sys.exit()

# cProfile.run("main()")

if __name__ == '__main__':
    main()